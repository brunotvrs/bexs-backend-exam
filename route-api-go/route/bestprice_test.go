package main

import (
	"strings"
	"testing"
)

func TestUpdateSearchModelInvalidParameter(t *testing.T) {
	ConfigTestDataSource()
	searchModel := UpdateSearchModelInvalidParameter()
	err := updateSearchModel(searchModel)
	if err == nil {
		t.Errorf("expected error: parameter with invalid format")
	}
}

func TestUpdateSearchModel(t *testing.T) {
	ConfigTestDataSource()
	searchModel := UpdateSearchModelTestStruct()
	err := updateSearchModel(searchModel)
	if err != nil {
		t.Errorf("not expected error: " + err.Error())
	}
	expectedFrom := "AAA"
	expectedTo := "BBB"
	if strings.Compare(expectedFrom, searchModel.From) != 0 {
		t.Errorf("not expected error: searchModel.From")
	}
	if strings.Compare(expectedTo, searchModel.To) != 0 {
		t.Errorf("not expected error: searchModel.To")
	}
	if len(searchModel.SingleRoutes) == 0 {
		t.Errorf("not expected error: empty routes")
	}
}

func TestSearchStartRoutes(t *testing.T) {
	ConfigTestDataSource()
	searchModel := SearchStartRoutesTestStruct()
	searchStartRoutes(searchModel)

	if len(searchModel.Possibilities) != 2 {
		t.Errorf("expected 2 possibilities")
	}

	for _, pos := range searchModel.Possibilities {
		if !(strings.Compare(pos, "AAA-BBB=valid_route") == 0 || strings.Compare(pos, "AAA-CCC") == 0) {
			t.Errorf("SearchStartRoutes made a mistake choosing the routes")
		}
	}
}

func TestSearchBestPriceRoute(t *testing.T) {
	ConfigTestDataSource()
	searchModel := SearchBestPriceRouteTestStruct()
	searchBestPriceRoute(searchModel)
	if strings.Compare(searchModel.BestPriceRoute, "Best route: AAA-BBB > $10.00") != 0 {
		t.Errorf("SearchBestPriceRoute made a mistake choosing the best route")
	}
}

func TestMultipleResults(t *testing.T) {
	ConfigTestDataSource()
	searchModel := MultipleResultsTestStruct()
	updateSearchModel(searchModel)
	searchStartRoutes(searchModel)
	searchConnections(searchModel)
	searchBestPriceRoute(searchModel)

	if strings.Compare(searchModel.BestPriceRoute, "Best route: QQQ-DDD > $20.00 OR QQQ-HHH-DDD > $20.00") != 0 {
		t.Errorf("the result should return more than one route")
	}
}
