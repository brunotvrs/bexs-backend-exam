package main

import (
	"encoding/json"
	"errors"
	"io"
	"net/http"
	"strconv"
	"strings"
)

type RouteModel struct {
	FromLocation string  `json:"fromLocation"`
	ToLocation   string  `json:"toLocation"`
	Price        float64 `json:"price"`
	FullRoute    string
}

func SaveUpdate(w http.ResponseWriter, r *http.Request) {
	var route RouteModel
	err := json.NewDecoder(r.Body).Decode(&route)
	if err != nil {
		http.Error(w, "invalid fields", http.StatusBadRequest)
		return
	}

	if CheckData(route.FromLocation + ";" + route.ToLocation + ";" + strconv.FormatFloat(route.Price, 'f', 2, 64)) {

		err = save(route)

		if err == nil {
			w.Write([]byte("Success!"))
		} else {
			http.Error(w, "error saving record", http.StatusInternalServerError)
			return
		}

	} else {
		http.Error(w, "invalid fields", http.StatusBadRequest)
		return
	}
}

func save(route RouteModel) error {
	str, update, err := tryToUpdate(route)

	if err != nil {
		return err
	}

	if !update {
		str = str + route.FromLocation + ";" + route.ToLocation + ";" + strconv.FormatFloat(route.Price, 'f', 2, 64)
	}

	err = Write(str)
	return err
}

func tryToUpdate(route RouteModel) (string, bool, error) {
	reader, err := Read()
	if err != nil {
		return "", false, err
	}

	var str string
	var update bool = false

	for {
		line, err := reader.Read()
		if err == io.EOF {
			break
		}
		if len(line) > 0 && CheckData(line[0]) {
			oldLine := strings.Split(line[0], ";")
			if oldLine[0] == route.FromLocation && oldLine[1] == route.ToLocation {
				update = true
				str = str + route.FromLocation + ";" + route.ToLocation + ";" + strconv.FormatFloat(route.Price, 'f', 2, 64) + "\r\n"
			} else {
				str = str + oldLine[0] + ";" + oldLine[1] + ";" + oldLine[2] + "\r\n"
			}
		} else {
			return "", false, errors.New("input-file contains invalid values: " + line[0])
		}
	}
	return str, update, nil
}

func ListRoutes() ([]RouteModel, error) {
	reader, err := Read()

	if err != nil {
		return nil, err
	}

	singleRoutes := make([]RouteModel, 0, 5)

	for {
		line, err := reader.Read()
		if err == io.EOF {
			break
		}

		if len(line) > 0 && CheckData(line[0]) {
			route := strings.Split(line[0], ";")
			price, _ := strconv.ParseFloat(route[2], 64)

			routeModel := RouteModel{
				FromLocation: route[0],
				ToLocation:   route[1],
				Price:        price,
				FullRoute:    route[0] + "-" + route[1],
			}
			singleRoutes = append(singleRoutes, routeModel)
		} else {
			return nil, errors.New("input-file is empty or contains invalid values")
		}
	}
	return singleRoutes, nil
}
