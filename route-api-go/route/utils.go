package main

import (
	"strings"
)

func Remove(slice []string, s string) []string {

	clone := make([]string, 0, 5)

	for _, item := range slice {

		if strings.Compare(item, s) != 0 {
			clone = append(clone, item)
		}
	}
	return clone
}

func Compare(a, b []string) bool {
	if len(a) != len(b) {
		return false
	}
	for _, v := range a {
		equal := false
		for _, k := range b {
			if strings.Compare(v, k) == 0 {
				equal = true
				break
			}
		}
		if !equal {
			return false
		}
	}
	return true
}

func AddAll(a, b []string) []string {

	for _, v := range b {
		if !Contains(a, v) {
			a = append(a, v)
		}
	}
	return a
}

func Contains(s []string, e string) bool {
	for _, a := range s {
		if a == e {
			return true
		}
	}
	return false
}

func AppendNotRepeated(a []string, b string) []string {

	if len(a) > 0 {
		if !Contains(a, b) {
			a = append(a, b)
		}
	} else {
		a = append(a, b)
	}
	return a
}
