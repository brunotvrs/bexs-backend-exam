package main

import (
	"errors"
	"net/http"
	"regexp"
	"strconv"
	"strings"

	"github.com/gorilla/mux"
)

type BestPriceRouteModel struct {
	From                string
	To                  string
	FromLocation        string
	ToLocation          string
	Price               float64
	FromTo              string
	ValidFromTo         string
	BestRoute           string
	BestPriceRoute      string
	BestPrice           float64
	ContenderRoute      string
	ContenderPrice      float64
	Possibilities       []string
	SelectedRoutes      []string
	ReadySelectedRoutes []string
	SingleRoutes        []RouteModel
}

func SearchBestPriceRoute(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	model := BestPriceRouteModel{
		FromTo: params["FromLocation"] + "-" + params["ToLocation"],
	}
	var searchModel *BestPriceRouteModel = &model
	err := updateSearchModel(searchModel)
	if err == nil {
		searchStartRoutes(searchModel)
		searchConnections(searchModel)
		if len(searchModel.ReadySelectedRoutes) > 0 {
			searchBestPriceRoute(searchModel)
			w.Write([]byte(searchModel.BestPriceRoute))
		} else {
			w.Write([]byte("No route available!"))
		}
	} else {
		w.Write([]byte(err.Error()))
	}
}

func updateSearchModel(searchModel *BestPriceRouteModel) error {
	searchModel.FromTo = strings.ToUpper(searchModel.FromTo)
	matched, err := regexp.MatchString("[a-zA-Z]{3}-[a-zA-Z]{3}", searchModel.FromTo)
	if matched && err == nil && len(searchModel.FromTo) == 7 {
		searchModel.From = strings.Split(searchModel.FromTo, "-")[0]
		searchModel.To = strings.Split(searchModel.FromTo, "-")[1]
		searchModel.ValidFromTo = searchModel.FromTo
		singleRoutes, err := ListRoutes()
		if err != nil {
			return err
		}
		searchModel.SingleRoutes = singleRoutes
	} else {
		return errors.New("parameter with invalid format")
	}
	return nil
}

func searchStartRoutes(searchModel *BestPriceRouteModel) {
	for _, singleRoute := range searchModel.SingleRoutes {

		if strings.Compare(singleRoute.FullRoute, searchModel.ValidFromTo) == 0 {
			searchModel.Possibilities = AppendNotRepeated(searchModel.Possibilities, singleRoute.FullRoute+"=valid_route")
		} else if strings.Compare(singleRoute.FromLocation, searchModel.From) == 0 {
			searchModel.Possibilities = AppendNotRepeated(searchModel.Possibilities, singleRoute.FullRoute)
		}
	}
}

func searchConnections(searchModel *BestPriceRouteModel) {
	nextListParent := make([]string, 0, 5)
	if len(searchModel.Possibilities) > 1 {
		for {
			if len(searchModel.Possibilities) == 0 {
				break
			}
			nextList := createRoutePossibilities(searchModel)
			if !Compare(nextList, nextListParent) {
				searchModel.Possibilities = AddAll(searchModel.Possibilities, nextList)
				possibilitiesClone := searchModel.Possibilities
				for _, pos := range possibilitiesClone {
					if strings.HasSuffix(pos, "=valid_route") {
						searchModel.SelectedRoutes = AppendNotRepeated(searchModel.SelectedRoutes, pos)
						searchModel.Possibilities = Remove(searchModel.Possibilities, pos)
					}
				}
				nextListParent = nextList
			} else {
				searchModel.Possibilities = searchModel.Possibilities[:0]
			}
		}
	} else if len(searchModel.Possibilities) == 1 && strings.HasSuffix(searchModel.Possibilities[0], "=valid_route") {
		searchModel.SelectedRoutes = AddAll(searchModel.SelectedRoutes, searchModel.Possibilities)
	}
	for _, selectedRoute := range searchModel.SelectedRoutes {
		searchModel.ReadySelectedRoutes = AppendNotRepeated(searchModel.ReadySelectedRoutes, strings.Split(selectedRoute, "=")[0])
	}
}

func createRoutePossibilities(searchModel *BestPriceRouteModel) []string {
	nextList := make([]string, 0, 5)
	for _, route := range searchModel.Possibilities {
		if !strings.HasSuffix(route, "=valid_route") {
			to := route[len(route)-3:]
			for _, singleRoute := range searchModel.SingleRoutes {
				routeCandidate := singleRoute.FullRoute
				if strings.HasPrefix(routeCandidate, to) && !strings.Contains(route, routeCandidate) {
					if strings.HasSuffix(routeCandidate, searchModel.To) {
						nextList = AppendNotRepeated(nextList, route+">"+routeCandidate+"=valid_route")
					} else {
						nextList = AppendNotRepeated(nextList, route+">"+routeCandidate)
					}
				}
			}
		} else {
			nextList = append(nextList, route)
		}
	}
	for _, ignoredRoute := range searchModel.SelectedRoutes {
		if Contains(nextList, ignoredRoute) {
			nextList = Remove(nextList, ignoredRoute)
		}
	}
	return nextList
}

func searchBestPriceRoute(searchModel *BestPriceRouteModel) {
	for _, route := range searchModel.ReadySelectedRoutes {
		searchModel.ContenderRoute = route
		for _, singleSelectedRoute := range strings.Split(route, ">") {
			for _, singleRoute := range searchModel.SingleRoutes {
				if strings.Compare(singleRoute.FullRoute, singleSelectedRoute) == 0 {
					if searchModel.ContenderPrice == 0.0 {
						searchModel.ContenderPrice = singleRoute.Price
					} else {
						searchModel.ContenderPrice = searchModel.ContenderPrice + singleRoute.Price
					}
				}
			}
		}
		if len(searchModel.BestRoute) == 0 || searchModel.ContenderPrice < searchModel.BestPrice {
			searchModel.BestRoute = searchModel.ContenderRoute
			searchModel.BestPrice = searchModel.ContenderPrice
		} else if searchModel.ContenderPrice == searchModel.BestPrice {
			searchModel.BestRoute = searchModel.BestRoute + "/" + searchModel.ContenderRoute
		}
		searchModel.ContenderPrice = 0.0
		searchModel.ContenderRoute = ""
	}
	formattingBestPriceRouteResult(searchModel)
}

func formattingBestPriceRouteResult(searchModel *BestPriceRouteModel) {

	if strings.Contains(searchModel.BestRoute, "/") {
		bestRoute := searchModel.BestRoute
		formattedRoute := ""
		check := true
		for _, route := range strings.Split(searchModel.BestRoute, "/") {
			searchModel.BestRoute = route
			formattingBestPriceRoute(searchModel)
			if check {
				formattedRoute = formattedRoute + searchModel.BestPriceRoute + " OR "
			} else {
				formattedRoute = formattedRoute + searchModel.BestPriceRoute[12:] + " OR "
			}
			check = false
		}

		searchModel.BestPriceRoute = formattedRoute
		searchModel.BestPriceRoute = searchModel.BestPriceRoute[0 : len(searchModel.BestPriceRoute)-4]
		searchModel.BestRoute = bestRoute
	} else {
		formattingBestPriceRoute(searchModel)
	}
}

func formattingBestPriceRoute(searchModel *BestPriceRouteModel) {
	check := true
	for _, route := range strings.Split(searchModel.BestRoute, ">") {
		if check {
			searchModel.BestPriceRoute = route + "-"
			check = false
		} else {
			searchModel.BestPriceRoute = searchModel.BestPriceRoute + strings.Split(route, "-")[1] + "-"
		}
	}
	searchModel.BestPriceRoute = "Best route: " + searchModel.BestPriceRoute[0:len(searchModel.BestPriceRoute)-1] + " > $" + strconv.FormatFloat(searchModel.BestPrice, 'f', 2, 64)
}
