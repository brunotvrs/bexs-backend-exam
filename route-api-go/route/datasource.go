package main

import (
	"bufio"
	"encoding/csv"
	"errors"
	"io/ioutil"
	"os"
	"regexp"
	"strings"
)

func Read() (*csv.Reader, error) {
	file, err := openFile()
	reader := csv.NewReader(bufio.NewReader(file))
	return reader, err
}

func Write(str string) error {
	dataSource, err := dataSourceArg()
	if err != nil {
		return err
	}
	err = ioutil.WriteFile(dataSource, []byte(str), 0644)
	return err
}

func dataSourceArg() (string, error) {
	if len(os.Args) >= 2 && strings.Contains(os.Args[1], "input-file") {
		return os.Args[1], nil
	} else {
		return "", errors.New("the data file path is not present in the execution arguments")
	}
}

func openFile() (*os.File, error) {
	dataSource, err := dataSourceArg()
	file, _ := os.Open(dataSource)
	return file, err
}

func CheckData(line string) bool {
	if strings.Compare("", line) != 0 {
		lineSplt := strings.Split(line, ";")
		matchedFrom, _ := regexp.MatchString("[a-zA-Z]{3}", lineSplt[0])
		matchedTo, _ := regexp.MatchString("[a-zA-Z]{3}", lineSplt[1])
		matchedPrice, _ := regexp.Match("[+-]?([0-9]*[.])?[0-9]+", []byte(lineSplt[2]))
		if len(lineSplt) == 3 && len(lineSplt[0]) == 3 && len(lineSplt[1]) == 3 && matchedFrom && matchedTo && matchedPrice {
			return true
		}
	}
	return false
}

func ConfigTestDataSource() {
	os.Args = make([]string, 2)
	os.Args[1] = "../test-input-file.csv"
}

func CleanConfigTestDataSource() {
	os.Args = make([]string, 2)
}

func UpdateSearchModelInvalidParameter() *BestPriceRouteModel {
	return &BestPriceRouteModel{FromTo: "AsAA-BBB"}
}

func UpdateSearchModelTestStruct() *BestPriceRouteModel {
	return &BestPriceRouteModel{FromTo: "AAA-BBB"}
}

func SearchStartRoutesTestStruct() *BestPriceRouteModel {
	singleRoutes, _ := ListRoutes()
	return &BestPriceRouteModel{
		FromTo:       "AAA-BBB",
		From:         "AAA",
		To:           "BBB",
		ValidFromTo:  "AAA-BBB",
		SingleRoutes: singleRoutes}
}

func SearchBestPriceRouteTestStruct() *BestPriceRouteModel {
	readySelectedRoutes := []string{"AAA-BBB", "AAA-CCC>CCC-BBB"}
	singleRoutes, _ := ListRoutes()
	return &BestPriceRouteModel{
		FromTo:              "AAA-BBB",
		From:                "AAA",
		To:                  "BBB",
		ValidFromTo:         "AAA-BBB",
		SingleRoutes:        singleRoutes,
		ReadySelectedRoutes: readySelectedRoutes,
	}
}

func MultipleResultsTestStruct() *BestPriceRouteModel {
	return &BestPriceRouteModel{FromTo: "QQQ-DDD"}
}

func SaveTestStruct() RouteModel {
	return RouteModel{FromLocation: "ZZZ",
		ToLocation: "YYY",
		Price:      10.0}
}

func UpdateStruct() RouteModel {
	return RouteModel{FromLocation: "AAA",
		ToLocation: "BBB",
		Price:      5.0}
}

func RestartTestDataSource() {
	ConfigTestDataSource()
	Write("AAA;BBB;10" + "\r\n" +
		"BBB;CCC;5" + "\r\n" +
		"AAA;CCC;75" + "\r\n" +
		"CCC;BBB;20" + "\r\n" +
		"QQQ;DDD;20" + "\r\n" +
		"QQQ;HHH;10" + "\r\n" +
		"HHH;DDD;10")
}
