package main

import (
	"net/http"

	"github.com/gorilla/mux"
)

func main() {

	router := mux.NewRouter()
	router.HandleFunc("/route-api/route", SaveUpdate).Methods("POST")
	router.HandleFunc("/route-api/best-price-route/fromLocation={FromLocation}&toLocation={ToLocation}", SearchBestPriceRoute).Methods("GET")
	http.ListenAndServe(":8080", router)
}
