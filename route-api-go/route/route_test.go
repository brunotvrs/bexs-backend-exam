package main

import (
	"strings"
	"testing"
)

func TestSave(t *testing.T) {
	ConfigTestDataSource()
	err := save(SaveTestStruct())

	if err != nil {
		t.Errorf("not expected error: " + err.Error())
	}
	RestartTestDataSource()
}

func TestUpdate(t *testing.T) {
	ConfigTestDataSource()
	updateStruct := UpdateStruct()
	err := save(updateStruct)
	if err != nil {
		t.Errorf("not expected error: " + err.Error())
	}

	routes, _ := ListRoutes()
	updateOk := false
	for _, route := range routes {
		if strings.Compare(updateStruct.FromLocation, route.FromLocation) == 0 &&
			strings.Compare(updateStruct.ToLocation, route.ToLocation) == 0 &&
			route.Price == 5.0 {
			updateOk = true
		}
	}

	if !updateOk {
		t.Errorf("not expected error: the update method did not update the record")
	}
	RestartTestDataSource()
}

func TestListRoutesNoDataFile(t *testing.T) {
	CleanConfigTestDataSource()
	_, err := ListRoutes()
	if err == nil {
		t.Errorf("expected error: input-file is empty or contains invalid values; but got nil")
	}
}

func TestListRoutes(t *testing.T) {
	ConfigTestDataSource()
	_, err := ListRoutes()
	if err != nil {
		t.Errorf(err.Error())
	}
}
