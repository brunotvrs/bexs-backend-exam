package com.bexs.route.core.common.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.bexs.route.core.common.enums.MessageCode;
import com.bexs.route.core.common.exception.ErrorCode;
import com.bexs.route.core.common.model.RouteModel;
import com.bexs.route.core.common.service.RouteService;

/**
 * Controlador contendo os endpoints responsáveis pelas operações de rota.
 *
 * @author bruno
 */
@RestController
public class RouteController {

    @Autowired
    private RouteService routeService;

    /**
     * Salva ou atualiza uma rota via POST (JSON no Body da requisição) Exemplo de
     * Body: { "fromLocation" : "AAA", "toLocation" : "BBB", "price" : 20 }
     *
     * @param routeModel <-- Criado quando recebe um request
     * @return httpResponse
     */
    @PostMapping(path = "/route-api/route")
    public ResponseEntity<String> newRoutePost(@RequestBody RouteModel routeModel) {

        if (routeModel != null && routeModel.getFromLocation() != null && routeModel.getToLocation() != null
                && routeModel.getPrice() != null) {
            final String response = this.routeService.saveUpdate(routeModel);
            if (response.startsWith("ERR-"))
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
            else if (response.startsWith("EXP-"))
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
            else
                return ResponseEntity.ok(MessageCode.SCS_1.getMessage());
        } else {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ErrorCode.ERR_5.toString());
        }
    }

}
