package com.bexs.route.core.common.model;

/**
 * Classe com um objeto modelo de rota. É utilizado para receber, validar e transacionar dados entre
 * camadas.
 * 
 * @author bruno
 *
 */
public class RouteModel {

    private String fromLocation;
    private String toLocation;
    private String price;

    public RouteModel() {
    }

    public RouteModel(String fromLocation, String toLocation, String price) {
        this.fromLocation = fromLocation;
        this.toLocation = toLocation;
        this.price = price;
    }

    public String getFromLocation() { return fromLocation; }

    public void setFromLocation(final String fromLocation) {
        this.fromLocation = fromLocation;
    }

    public String getToLocation() {
        return toLocation;
    }

    public void setToLocation(final String toLocation) {
        this.toLocation = toLocation;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }
}
