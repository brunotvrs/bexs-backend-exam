package com.bexs.route.core.common.util;

import org.springframework.boot.ApplicationArguments;

public class CoreUtil {

    /**
     * Recupera o caminho do arquivo contendo os dados de rotas. O caminho é enviado
     * como argumento ao rodar a aplicação.
     *
     * @return dataSource
     */
    public String getDataSource(final ApplicationArguments applicationArguments) {
        final String[] args = applicationArguments.getSourceArgs();
        if (args != null && args.length > 0)
            return args[0];
        // Para apresentar os testes unitários
        return getClass().getResource("../../../../../../static/test-data.txt").getPath();
    }

}
