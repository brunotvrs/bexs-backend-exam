package com.bexs.route.core.common.enums;

/**
 * Enum para agrupar as mensagens do sitema.
 *
 * @author bruno
 */
public enum MessageCode {

    SCS_1("SCS-1", "Success!"), SCS_2("SCS-2", "Ok!"), SCS_3("SCS-3", "No route available!");

    private final String code;
    private final String message;

    MessageCode(final String code, final String message) {
        this.code = code;
        this.message = message;
    }

    @Override
    public String toString() {
        return code + ": " + message;
    }

    public String getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

}
