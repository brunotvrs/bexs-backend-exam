package com.bexs.route.core.common.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.stereotype.Service;

import com.bexs.route.core.common.enums.MessageCode;
import com.bexs.route.core.common.exception.BusinessException;
import com.bexs.route.core.common.exception.ErrorCode;
import com.bexs.route.core.common.model.SearchBestPriceModel;
import com.bexs.route.core.common.util.CoreUtil;
import com.bexs.route.persistence.dao.impl.RouteDaoDataSourceImpl;
import com.bexs.route.persistence.entity.Route;
import com.bexs.route.persistence.exception.DataAccessException;

@Service
public class SearchService {

	@Autowired
	private ApplicationArguments applicationArguments;
	private CoreUtil coreUtil = new CoreUtil();

	/**
	 * Procura pela rota mais barata recebendo a infomação de partida e chegada.
	 * 
	 * @param searchModel
	 * @return response
	 */
	public String searchBestRoute(SearchBestPriceModel searchModel) {

		try {
			searchModel = updateSearchModel(searchModel);
			searchStartRoutes(searchModel);
			searchConnections(searchModel);
			if (!searchModel.getReadySelectedRoutes().isEmpty()) {
				searchBestPriceRoute(searchModel);
				return searchModel.getBestPriceRoute();
			} else {
				return MessageCode.SCS_3.getMessage();
			}
		} catch (final BusinessException b) {
			return b.getErrorCode().toString();
		} catch (final DataAccessException dataException) {
			return dataException.getErrorCode().toString();
		}
	}

	/**
	 * Atualiza e valida as informações recebida do controlador. Retorna o objeto
	 * preenchido com os dados obrigatórios para a realização da pesquisa.
	 * 
	 * @param searchModel
	 * @return searchModel
	 * @throws BusinessException
	 */
	private SearchBestPriceModel updateSearchModel(final SearchBestPriceModel searchModel) throws BusinessException {
		searchModel.setValidFromTo(validateFromTo(searchModel.getFromTo()));
		searchModel.setSingleRoutes(new RouteDaoDataSourceImpl().list(coreUtil.getDataSource(applicationArguments)));
		return searchModel;
	}

	/**
	 * Valida e corrige os dados de entrada.
	 * 
	 * @param fromTo
	 * @return validFromTo
	 * @throws BusinessException
	 */
	private String validateFromTo(String fromTo) throws BusinessException {
		fromTo = fromTo.toUpperCase().replace("/n", "").replace("/r", "").trim();
		if (fromTo.matches("[a-zA-Z]{3}-[a-zA-Z]{3}"))
			return fromTo;
		else
			throw new BusinessException(ErrorCode.ERR_1);
	}

	/**
	 * Procura todas as opções de primeiro trecho com a rota de partida. Atualiza a
	 * lista de possibilidades do searchModel. Um marcador é adicionado caso uma
	 * rota direta seja encontrada.
	 * 
	 * @param searchModel
	 */
	private void searchStartRoutes(final SearchBestPriceModel searchModel) {
		for (final Route singleRoute : searchModel.getSingleRoutes()) {
			if (singleRoute.getFullRoute().equals(searchModel.getValidFromTo())) {
				searchModel.getPossibilities().add(singleRoute.getFullRoute() + "=valid_route");
			} else if (singleRoute.getFromLocation().equals(searchModel.getFrom())) {
				searchModel.getPossibilities().add(singleRoute.getFullRoute());
			}
		}
	}

	/**
	 * Método recursivo para encontrar as conexões partindo das rotas selecionadas
	 * como iniciais. Um marcador é adicionado caso uma rota destino seja
	 * encontrada. O método é encerrado quando todas as possibilidades de combinação
	 * entre trechos com o local de partida e o local de chegada são encontradas.
	 * 
	 * @param searchModel
	 */
	private void searchConnections(final SearchBestPriceModel searchModel) {
		Set<String> nextListParent = new HashSet<>();

		if (searchModel.getPossibilities().size() > 1) {
			while (!searchModel.getPossibilities().isEmpty()) {
				final Set<String> nextList = createRoutePossibilities(searchModel.getSingleRoutes(),
						searchModel.getPossibilities(), searchModel.getTo(), searchModel.getSelectedRoutes());
				if (!nextList.equals(nextListParent)) {
					searchModel.getPossibilities().addAll(nextList);
					final List<String> possibilitiesClone = new ArrayList<>(searchModel.getPossibilities());
					for (final String pos : possibilitiesClone) {
						if (pos.endsWith("=valid_route")) {
							searchModel.getSelectedRoutes().add(pos);
							searchModel.getPossibilities().remove(pos);
						}
					}
					nextListParent = nextList;
				} else
					searchModel.getPossibilities().clear();
			}
		} else if (searchModel.getPossibilities().size() == 1
				&& searchModel.getPossibilities().iterator().next().endsWith("=valid_route")) {
			searchModel.setSelectedRoutes(searchModel.getPossibilities());
		}
		for (final String selectedRoute : searchModel.getSelectedRoutes())
			searchModel.getReadySelectedRoutes().add(selectedRoute.split("=")[0]);
	}

	/**
	 * Chamado recursivamente, esse método monta as novas possibilidades de rota
	 * partindo de cada trecho que espera combinação.
	 * 
	 * @param singleRoutes
	 * @param possibilities
	 * @param endTo
	 * @param ignoredRoutes
	 * @return possibilities
	 */
	private Set<String> createRoutePossibilities(final List<Route> singleRoutes, final Set<String> possibilities,
			final String endTo, final Set<String> ignoredRoutes) {

		final Set<String> nextList = new HashSet<>();
		for (final String route : possibilities) {
			final String to = route.substring(route.length() - 3, route.length());
			for (final Route singleRoute : singleRoutes) {
				String routeCandidate = singleRoute.getFullRoute();
				if (routeCandidate.startsWith(to) && !route.contains(routeCandidate)) {
					if (routeCandidate.endsWith(endTo))
						nextList.add(route + ">" + routeCandidate + "=valid_route");
					else
						nextList.add(route + ">" + routeCandidate);
				}
			}
		}
		for (final String ignoredRoute : ignoredRoutes) {
			if (nextList.contains(ignoredRoute))
				nextList.remove(ignoredRoute);
		}
		return nextList;
	}

	/**
	 * Compara o preço entre as rotas selecionadas. Existem duas possibilidades de
	 * saída: Um trecho mais barato ou vários trechos mais baratos com o mesmo
	 * valor.
	 * 
	 * @param searchModel
	 */
	private void searchBestPriceRoute(final SearchBestPriceModel searchModel) {

		for (final String route : searchModel.getReadySelectedRoutes()) {
			searchModel.setContenderRoute(route);
			for (final String singleSelectedRoute : Arrays.asList(route.split(">"))) {
				for (final Route singleRoute : searchModel.getSingleRoutes()) {
					if (singleRoute.getFullRoute().equals(singleSelectedRoute)) {
						if (searchModel.getContenderPrice() == null)
							searchModel.setContenderPrice(singleRoute.getPrice().doubleValue());
						else
							searchModel.setContenderPrice(
									singleRoute.getPrice().doubleValue() + searchModel.getContenderPrice());
					}
				}
			}
			if (searchModel.getBestRoute() == null || searchModel.getBestRoute().isEmpty()
					|| searchModel.getContenderPrice().doubleValue() < searchModel.getBestPrice().doubleValue()) {
				searchModel.setBestRoute(searchModel.getContenderRoute());
				searchModel.setBestPrice(searchModel.getContenderPrice());
			} else if (searchModel.getContenderPrice().doubleValue() == searchModel.getBestPrice().doubleValue()) {
				searchModel.setBestRoute(searchModel.getBestRoute() + "/" + searchModel.getContenderRoute());
			}
			searchModel.setContenderPrice(0D);
			searchModel.setContenderRoute("");
		}
		formattingBestPriceRouteResult(searchModel);
	}

	/**
	 * Formata a saída para uma rota de menor preço.
	 * 
	 * @param searchModel
	 */
	private void formattingBestPriceRoute(final SearchBestPriceModel searchModel) {
		boolean check = true;
		for (final String route : Arrays.asList(searchModel.getBestRoute().split(">"))) {
			if (check) {
				searchModel.setBestPriceRoute(route + "-");
				check = false;
			} else
				searchModel.setBestPriceRoute(searchModel.getBestPriceRoute() + route.split("-")[1] + "-");
		}
		searchModel.setBestPriceRoute("Best route: "
				+ searchModel.getBestPriceRoute().substring(0, searchModel.getBestPriceRoute().length() - 1) + " > $"
				+ searchModel.getBestPrice());
	}

	/**
	 * Formata a saída para várias rotas de menor preço.
	 * 
	 * @param searchModel
	 */
	private void formattingBestPriceRouteResult(final SearchBestPriceModel searchModel) {
		if (searchModel.getBestRoute().contains("/")) {

			final String bestRoute = searchModel.getBestRoute();
			var formattedRoute = "";
			boolean check = true;
			for (final String route : Arrays.asList(searchModel.getBestRoute().split("/"))) {
				searchModel.setBestRoute(route);
				formattingBestPriceRoute(searchModel);
				if (check) {
					formattedRoute += searchModel.getBestPriceRoute() + " OR ";
				} else {
					formattedRoute += searchModel.getBestPriceRoute().substring(12) + " OR ";
				}
				check = false;
			}
			searchModel.setBestPriceRoute(formattedRoute);
			searchModel.setBestPriceRoute(
					searchModel.getBestPriceRoute().substring(0, searchModel.getBestPriceRoute().length() - 4));
			searchModel.setBestRoute(bestRoute);
		} else {
			formattingBestPriceRoute(searchModel);
		}
	}

}
