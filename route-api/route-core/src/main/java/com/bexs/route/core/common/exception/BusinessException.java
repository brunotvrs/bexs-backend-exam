package com.bexs.route.core.common.exception;

import java.io.Serial;
import java.util.Arrays;
import java.util.List;

/**
 * Classe para exceções de negócio.
 *
 * @author bruno
 */
public class BusinessException
        extends RuntimeException {

    @Serial
    private static final long serialVersionUID = -8994646936897638508L;
    private final ErrorCode errorCode;
    private Object[] parameters;


    public BusinessException(final ErrorCode errorCode) {
        super(errorCode.getMessage());
        this.errorCode = errorCode;
    }

    public BusinessException(final ErrorCode errorCode, final Object... parameters) {
        super(errorCode.getMessage());
        this.errorCode = errorCode;
        this.parameters = parameters;
    }

    public BusinessException(final ErrorCode errorCode, final Throwable cause, final Object... parameters) {
        super(errorCode.getMessage(), cause);
        this.errorCode = errorCode;
        this.parameters = parameters;
    }

    public final ErrorCode getErrorCode() {
        return errorCode;
    }

    public List<Object> getParameters() {

        if (parameters != null) {
            return Arrays.asList(parameters);
        }

        return null;
    }

    public void setParameters(Object[] parameters) {
        this.parameters = parameters;
    }
}
