package com.bexs.route.core.common.service;

import java.math.BigDecimal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.stereotype.Service;

import com.bexs.route.core.common.enums.MessageCode;
import com.bexs.route.core.common.exception.BusinessException;
import com.bexs.route.core.common.exception.ErrorCode;
import com.bexs.route.core.common.model.RouteModel;
import com.bexs.route.core.common.util.CoreUtil;
import com.bexs.route.persistence.dao.impl.RouteDaoDataSourceImpl;
import com.bexs.route.persistence.entity.Route;
import com.bexs.route.persistence.exception.DataAccessException;

/**
 * Classe de serviço que executa operações de negócio relacionados ao objeto
 * Rota. Recebe e valida informações dos controladores, consulta a camada de
 * dados (se necessário) e persiste informações retornando o que foi
 * requisitado.
 *
 * @author bruno
 */
@Service
public class RouteService {

    @Autowired
    private ApplicationArguments applicationArguments;
    private final CoreUtil coreUtil = new CoreUtil();

    /**
     * Salva ou atualiza uma rota.
     *
     * @param routeModel <-- Rota recebida do controller
     * @return response
     */
    public String saveUpdate(final RouteModel routeModel) {

        try {
            // Escolho a implementação de acesso aos dados.
            final String dataSource = coreUtil.getDataSource(applicationArguments);
            if (dataSource != null) {
                new RouteDaoDataSourceImpl().saveUpdate(validateRouteDto(routeModel), dataSource);
                return MessageCode.SCS_1.getMessage();
            } else
                return ErrorCode.EXP_2.getMessage();
        } catch (final BusinessException businessException) {
            return businessException.getErrorCode().toString();
        } catch (final DataAccessException dataException) {
            return dataException.getErrorCode().toString();
        }
    }

    /**
     * Valida os dados recebidos por um modelo e retorna umas entidade pronta para
     * as próximas operações.
     *
     * @param routeModel <-- Rota recebida para a validação
     * @return route
     * @throws BusinessException <-- Exceção para o tratamento dos dados
     */
    public Route validateRouteDto(final RouteModel routeModel) throws BusinessException {

        // Testo os padrões e obrigatoriedade dos campos
        if (routeModel != null) {
            if (routeModel.getFromLocation() == null || !routeModel.getFromLocation().matches("[A-Z]{3}")) {
                throw new BusinessException(ErrorCode.ERR_2);
            } else if (routeModel.getToLocation() == null || !routeModel.getToLocation().matches("[A-Z]{3}")) {
                throw new BusinessException(ErrorCode.ERR_3);
            }

            try {
                new BigDecimal(routeModel.getPrice().replace(",", "."));

            } catch (Exception e) {
                throw new BusinessException(ErrorCode.ERR_4);
            }
        } else
            throw new BusinessException(ErrorCode.ERR_1);

        // Retorno uma entidade válida
        return new Route(routeModel.getFromLocation(), routeModel.getToLocation(),
                new BigDecimal(routeModel.getPrice().replace(",", ".")));
    }

}
