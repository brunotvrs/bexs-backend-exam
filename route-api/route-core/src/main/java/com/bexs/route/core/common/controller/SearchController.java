package com.bexs.route.core.common.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.bexs.route.core.common.model.SearchBestPriceModel;
import com.bexs.route.core.common.service.SearchService;

/**
 * Controlador contendo os endpoints responsáveis pelas operações de busca.
 *
 * @author bruno
 */
@RestController
public class SearchController {

    @Autowired
    private SearchService searchService;

    /**
     * Procura pela rota mais barata via GET (parâmetro na URL) Exemplo requisição:
     * http://localhost:8080/route-api/best-price-route/fromLocation=AAA&toLocation=BBB
     *
     * @param fromTo <-- Pesquisa com partida e destino
     * @return httpResponse
     */
    @GetMapping(path = "/route-api/best-price-route/fromLocation={fromLocation}&toLocation={toLocation}")
    public ResponseEntity<String> searchBestPriceRouteGet(@PathVariable("fromLocation") String fromLocation,
            @PathVariable("toLocation") String toLocation) {

        final SearchBestPriceModel searchModel = new SearchBestPriceModel();
        searchModel.setFromTo(fromLocation + "-" + toLocation);
        final String response = this.searchService.searchBestRoute(searchModel);
        if (response.startsWith("ERR-"))
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
        else if (response.startsWith("EXP-"))
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
        else
            return ResponseEntity.ok(response);

    }
    
}
