package com.bexs.route.core.common.exception;

/**
 * Enum para agrupar as mensagens de erro uilizadas nas exceções.
 *
 * @author bruno
 */
public enum ErrorCode {

    EXP_1("EXP-1", "Internal error."),
    EXP_2("EXP-2", "The data file path is not present in the execution arguments."),
    EXP_3("EXP-3", "Data file not found or does not match the execution requirements."),
    EXP_4("EXP-4", "Data file path is not present in the execution arguments."),
    ERR_1("ERR-1", "Formatting error: Try the pattern \"fromLocation=AAA&toLocation=BBB\""),
    ERR_2("ERR-2", "Formatting error: Field \"fromLocation\" incorrect! Try: \"AAA\""),
    ERR_3("ERR-3", "Formatting error: Field \"toLocation\" incorrect! Try: \"AAA\""),
    ERR_4("ERR-4", "Formatting error: Field \"price\" incorrect! Try: \"40\", \"40.0\" or \"40,00\""),
    ERR_5("ERR-5", "Formatting error: Malformed JSON. Check keys and values");

    private final String code;
    private final String message;

    ErrorCode(final String code, final String message) {
        this.code = code;
        this.message = message;
    }

    @Override
    public String toString() {
        return code + ": " + message;
    }

    public String getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

}
