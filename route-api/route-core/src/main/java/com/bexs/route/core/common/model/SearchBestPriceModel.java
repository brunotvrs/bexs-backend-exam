package com.bexs.route.core.common.model;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.bexs.route.persistence.entity.Route;

/**
 * Classe com um objeto modelo de pesquisa pelo menor preço. É utilizado para
 * receber, validar e transacionar dados entre camadas.
 *
 * @author bruno
 */
public class SearchBestPriceModel {

    private String fromTo;
    private String validFromTo;
    private String bestRoute;
    private String bestPriceRoute;
    private Double bestPrice;
    private String contenderRoute;
    private Double contenderPrice;
    private List<Route> singleRoutes = new ArrayList<>();
    private Set<String> possibilities = new HashSet<>();
    private Set<String> selectedRoutes = new HashSet<>();
    private Set<String> nextListParent = new HashSet<>();
    private Set<String> readySelectedRoutes = new HashSet<>();

    public SearchBestPriceModel() {
    }

    public String getFromTo() {
        return fromTo;
    }

    public void setFromTo(String fromTo) {
        this.fromTo = fromTo;
    }

    public String getValidFromTo() {
        return validFromTo;
    }

    public void setValidFromTo(String validFromTo) {
        this.validFromTo = validFromTo;
    }

    public String getBestRoute() {
        return bestRoute;
    }

    public void setBestRoute(String bestRoute) {
        this.bestRoute = bestRoute;
    }

    public String getBestPriceRoute() {
        return bestPriceRoute;
    }

    public void setBestPriceRoute(String bestPriceRoute) {
        this.bestPriceRoute = bestPriceRoute;
    }

    public Double getBestPrice() {
        return bestPrice;
    }

    public void setBestPrice(Double bestPrice) {
        this.bestPrice = bestPrice;
    }

    public String getContenderRoute() {
        return contenderRoute;
    }

    public void setContenderRoute(String contenderRoute) {
        this.contenderRoute = contenderRoute;
    }

    public Double getContenderPrice() {
        return contenderPrice;
    }

    public void setContenderPrice(Double contenderPrice) {
        this.contenderPrice = contenderPrice;
    }

    public String getFrom() {
        if (validFromTo != null)
            return validFromTo.split("-")[0];
        else
            return null;
    }

    public String getTo() {
        if (validFromTo != null)
            return validFromTo.split("-")[1];
        else
            return null;
    }

    public List<Route> getSingleRoutes() {
        return singleRoutes;
    }

    public void setSingleRoutes(List<Route> singleRoutes) {
        this.singleRoutes = singleRoutes;
    }

    public Set<String> getPossibilities() {
        return possibilities;
    }

    public void setPossibilities(Set<String> possibilities) {
        this.possibilities = possibilities;
    }

    public Set<String> getSelectedRoutes() {
        return selectedRoutes;
    }

    public void setSelectedRoutes(Set<String> selectedRoutes) {
        this.selectedRoutes = selectedRoutes;
    }

    public Set<String> getNextListParent() {
        return nextListParent;
    }

    public void setNextListParent(Set<String> nextListParent) {
        this.nextListParent = nextListParent;
    }

    public Set<String> getReadySelectedRoutes() {
        return readySelectedRoutes;
    }

    public void setReadySelectedRoutes(Set<String> readySelectedRoutes) {
        this.readySelectedRoutes = readySelectedRoutes;
    }
}
