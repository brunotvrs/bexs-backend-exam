package com.bexs.route.core;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.bexs.route.core.common.exception.ErrorCode;
import com.bexs.route.persistence.dao.util.FileDataSource;

/**
 * Classe contendo o método main que inicia sistema.
 *
 * @author bruno
 */
@SpringBootApplication
public class CoreApplication {

    /**
     * Como primeiro argumento de execução recebe o caminho físico com a fonte de
     * dados. Exemplo de argumento "C:\route\input-file.csv"
     *
     * @param args <-- Recebido da execução da aplicação (utilizamos apenas o primeiro argumento)
     */
    public static void main(String[] args) {

        if (args != null && args.length > 0 && !args[0].isEmpty()) {
            final String dataSourcePath = args[0].trim();
            if (!FileDataSource.validDatasourceFile(dataSourcePath))
                System.out.println(ErrorCode.EXP_3.toString());
            else
                SpringApplication.run(CoreApplication.class, args);
        } else
            System.out.println(ErrorCode.EXP_4.toString());
    }

}
