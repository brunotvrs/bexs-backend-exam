package com.bexs.route.core.common.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

/**
 * Classe de teste do Controller de buscas. Os métodos seguem o padrão de
 * nomenclatura "resultadoEsperado_quandoExecuto()".
 *
 * @author bruno
 */
@SpringBootTest
@AutoConfigureMockMvc
class SearchControllerTest {

    @Autowired
    MockMvc mockMvc;


    /**
     * Testa a resposta HTTP quando uma busca válida por rota de melhor preço é realizada.
     */
    @Test
    public void httpStatusOk_searchBestPriceRoute() {

        try {
            mockMvc.perform(get("/route-api/best-price-route/fromLocation=AAA&toLocation=BBB"))
                    .andExpect(status().isOk());
        } catch (final Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Testa a resposta HTTP quando uma busca inválida por rota de melhor preço é realizada.
     */
    @Test
    public void httpStatusBadRequest_searchBestPriceRoute() {

        try {
            mockMvc.perform(get("/route-api/best-price-route/fromLocation=AsAA&toLocation=BBB"))
                    .andExpect(status().isBadRequest());
        } catch (final Exception e) {
            e.printStackTrace();
        }
    }

}
