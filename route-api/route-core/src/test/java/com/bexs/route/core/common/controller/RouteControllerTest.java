package com.bexs.route.core.common.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import com.bexs.route.core.common.model.RouteModel;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Classe de teste do controlador de rota. Os métodos seguem o padrão de
 * nomenclatura "resultadoEsperado_quandoExecuto()".
 *
 * @author bruno
 */
@SpringBootTest
@AutoConfigureMockMvc
class RouteControllerTest {

    @Autowired
    MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;
    

    /**
     * Testa a resposta HTTP quando uma rota VÁLIDA é salva.
     */
    @Test
    public void httpStatusOk_newRoute() {

        try {
            mockMvc.perform(post("/route-api/route").contentType("application/json")
                    .content(objectMapper.writeValueAsString(new RouteModel("AAA", "BBB", "40"))))
                    .andExpect(status().isOk());
        } catch (final Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Testa a resposta HTTP quando uma rota INVÁLIDA é salva.
     */
    @Test
    public void httpStatusBadRequest_newRoute() {

        try {
            mockMvc.perform(post("/route-api/route").contentType("application/json")
                    .content(objectMapper.writeValueAsString(new RouteModel("AAAa", "BBB", "40"))))
                    .andExpect(status().isBadRequest());
        } catch (final Exception e) {
            e.printStackTrace();
        }
    }
}