package com.bexs.route.core.common.service;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.bexs.route.core.common.enums.MessageCode;
import com.bexs.route.core.common.exception.ErrorCode;
import com.bexs.route.core.common.model.RouteModel;

import io.restassured.module.mockmvc.RestAssuredMockMvc;

/**
 * Classe de teste do Service de rotas. Os métodos seguem o padrão de
 * nomenclatura "resultadoEsperado_quandoExecuto()".
 * 
 * @author bruno
 */
@SpringBootTest
public class RouteServiceTest {

	@Autowired
	RouteService routeService;

	@BeforeEach
	public void setup() {
		RestAssuredMockMvc.standaloneSetup(this.routeService);
	}

	/**
	 * Testa o retorno bem sucedido da inserção de uma nova rota.
	 */
	@Test
	public void successMessage_saveNewRoute() {
		final String response = routeService.saveUpdate(new RouteModel("AAA", "BBB", "40"));
		Assertions.assertEquals(response, MessageCode.SCS_1.getMessage());
	}

	/**
	 * Testa a resposta quando "from" é inválido.
	 */
	@Test
	public void errorMessageFrom_saveNewRoute() {
		String response = routeService.saveUpdate(new RouteModel("AAsA", "BBB", "40"));
		Assertions.assertEquals(response, ErrorCode.ERR_2.toString());
		response = routeService.saveUpdate(new RouteModel("", "BBB", "40"));
		Assertions.assertEquals(response, ErrorCode.ERR_2.toString());
	}

	/**
	 * Testa a resposta quando "to" é inválido.
	 */
	@Test
	public void errorMessageTo_saveNewRoute() {
		String response = routeService.saveUpdate(new RouteModel("AAA", "BsBB", "40"));
		Assertions.assertEquals(response, ErrorCode.ERR_3.toString());
		response = routeService.saveUpdate(new RouteModel("AAA", "", "40"));
		Assertions.assertEquals(response, ErrorCode.ERR_3.toString());

	}

	/**
	 * Testa a resposta quando "price" é inválido.
	 */
	@Test
	public void errorMessagePrice_saveNewRoute() {
		String response = routeService.saveUpdate(new RouteModel("AAA", "BBB", "4s0"));
		Assertions.assertEquals(response, ErrorCode.ERR_4.toString());
		response = routeService.saveUpdate(new RouteModel("AAA", "BBB", ""));
		Assertions.assertEquals(response, ErrorCode.ERR_4.toString());
	}

}