package com.bexs.route.core.common.service;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.bexs.route.core.common.enums.MessageCode;
import com.bexs.route.core.common.exception.ErrorCode;
import com.bexs.route.core.common.model.SearchBestPriceModel;

import io.restassured.module.mockmvc.RestAssuredMockMvc;

/**
 * Classe de teste do Service de buscas. Os métodos seguem o padrão de
 * nomenclatura "resultadoEsperado_quandoExecuto()".
 * 
 * @author bruno
 */
@SpringBootTest
class SearchServiceTest {

	@Autowired
	SearchService searchService;

	@BeforeEach
	public void setup() {
		RestAssuredMockMvc.standaloneSetup(this.searchService);
	}

	/**
	 * Testa resposta não nula quando a busca de rota com melhor preço é acionada.
	 */
	@Test
	public void notNull_searchBestPriceRoute() {

		final SearchBestPriceModel searchModel = new SearchBestPriceModel();
		searchModel.setFromTo("AAA-BBB");
		final String response = searchService.searchBestRoute(searchModel);
		Assertions.assertNotNull(response);
	}

	/**
	 * Testa resposta quando uma rota não é encontrada.
	 */
	@Test
	public void noRouteAvaliabe_searchBestPriceRoute() {

		final SearchBestPriceModel searchModel = new SearchBestPriceModel();
		searchModel.setFromTo("AAA-BBB");
		final String response = searchService.searchBestRoute(searchModel);
		Assertions.assertEquals(response, MessageCode.SCS_3.getMessage());
	}

	/**
	 * Testa resposta quando uma rota é encontrada.
	 */
	@Test
	public void routeAvaliabe_searchBestPriceRoute() {

		final SearchBestPriceModel searchModel = new SearchBestPriceModel();
		searchModel.setFromTo("GRU-ORL");
		final String response = searchService.searchBestRoute(searchModel);
		Assertions.assertTrue(response.contains("Best route:"));
	}
	
	/**
	 * Testa resposta quando a entrada é incorreta.
	 */
	@Test
	public void notValidParam_searchBestPriceRoute() {

		final SearchBestPriceModel searchModel = new SearchBestPriceModel();
		searchModel.setFromTo("AAsA-BBB");
		final String response = searchService.searchBestRoute(searchModel);
		Assertions.assertEquals(response, ErrorCode.ERR_1.toString());
	}
	
	/**
	 * Testa resposta quando a entrada esta vazia.
	 */
	@Test
	public void emptyParam_searchBestPriceRoute() {

		final SearchBestPriceModel searchModel = new SearchBestPriceModel();
		searchModel.setFromTo("");
		final String response = searchService.searchBestRoute(searchModel);
		Assertions.assertEquals(response, ErrorCode.ERR_1.toString());
	}

}
