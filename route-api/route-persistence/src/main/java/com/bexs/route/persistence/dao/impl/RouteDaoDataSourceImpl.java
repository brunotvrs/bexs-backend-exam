package com.bexs.route.persistence.dao.impl;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.bexs.route.persistence.dao.RouteDAO;
import com.bexs.route.persistence.entity.Route;
import com.bexs.route.persistence.exception.DataAccessException;
import com.bexs.route.persistence.exception.ErrorCode;

/**
 * Classe de implementação de um acesso aos dados. Essa classa acessa dados em
 * um arquivo com as informações de rotas.
 *
 * @author bruno
 */
public class RouteDaoDataSourceImpl implements RouteDAO {

    /**
     * Salva ou atualiza um registro no arquivo.
     *
     * @param route      <-- Entity de rota
     * @param dataSource <-- Endereço apra coleta de dados
     */
    @Override
    public void saveUpdate(final Route route, final String dataSource) throws DataAccessException {

        try {
            final BufferedReader reader = new BufferedReader(new FileReader(dataSource));
            final StringBuilder sb = new StringBuilder();
            String line;
            boolean update = false;

            while ((line = reader.readLine()) != null) {

                final String[] oldRouteStr = line.split(";");
                if (oldRouteStr[0].equals(route.getFromLocation()) && oldRouteStr[1].equals(route.getToLocation())) {
                    update = true;
                    sb.append(route.getFromLocation() + ";" + route.getToLocation() + ";" + route.getPrice() + "\r\n");
                } else {
                    sb.append(line + "\r\n");
                }
            }

            if (!update) {
                final String routeStr = route.getFromLocation() + ";" + route.getToLocation() + ";" + route.getPrice();
                sb.append(routeStr);
            }
            reader.close();
            final BufferedWriter writer = new BufferedWriter(new FileWriter(dataSource));
            writer.write(sb.toString());
            writer.close();
        } catch (final FileNotFoundException fileNotFound) {
            throw new DataAccessException(ErrorCode.EXP_2);
        } catch (final IOException io) {
            throw new DataAccessException(ErrorCode.EXP_3);
        } catch (final Exception e) {
            throw new DataAccessException(ErrorCode.EXP_1);
        }
    }

    /**
     * Lista todas as rotas presentes no arquivo.
     *
     * @param dataSource <-- Endereço apra coleta de dados
     * @return routes
     */
    @Override
    public List<Route> list(final String dataSource) throws DataAccessException {

        final List<Route> routes = new ArrayList<>();
        try {
            final BufferedReader reader = new BufferedReader(new FileReader(dataSource));
            String line = reader.readLine();

            while (line != null) {
                String[] routeStr = line.split(";");
                if (routeStr.length == 3 && routeStr[0].matches("[a-zA-Z]{3}") && routeStr[1].matches("[a-zA-Z]{3}")) {
                    final Route route = new Route(routeStr[0], routeStr[1], new BigDecimal(routeStr[2]));
                    if (!routes.contains(route))
                        routes.add(route);
                    else if (routes.get(routes.indexOf(route)).getPrice().doubleValue() > route.getPrice()
                            .doubleValue())
                        routes.get(routes.indexOf(route)).setPrice(route.getPrice());
                } else {
                    reader.close();
                    throw new DataAccessException(ErrorCode.ERR_2);
                }
                line = reader.readLine();
            }
            reader.close();
        } catch (final Exception e) {
            if (e.getClass() == DataAccessException.class) {
                final DataAccessException dataAccessException = (DataAccessException) e;
                throw new DataAccessException(dataAccessException.getErrorCode());
            } else
                throw new DataAccessException(ErrorCode.EXP_2);
        }
        return routes;
    }
}
