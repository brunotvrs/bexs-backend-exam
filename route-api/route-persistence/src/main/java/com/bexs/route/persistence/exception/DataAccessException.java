package com.bexs.route.persistence.exception;

import java.io.Serial;
import java.util.Arrays;
import java.util.List;

/**
 * Classe para exceções referentes às transações de dados.
 *
 * @author bruno
 */
public class DataAccessException extends RuntimeException {

    @Serial
    private static final long serialVersionUID = -8994646936897638508L;
    private final ErrorCode errorCode;
    private Object[] parameters;

    public DataAccessException(final ErrorCode errorCode) {
        super(errorCode.getMessage());
        this.errorCode = errorCode;
    }

    public DataAccessException(final ErrorCode errorCode, final Object... parameters) {
        super(errorCode.getMessage());
        this.errorCode = errorCode;
        this.parameters = parameters;
    }

    public DataAccessException(final ErrorCode errorCode, final Throwable cause, final Object... parameters) {
        super(errorCode.getMessage(), cause);
        this.errorCode = errorCode;
        this.parameters = parameters;
    }

    public final ErrorCode getErrorCode() {
        return errorCode;
    }

    public List<Object> getParameters() {

        if (parameters != null) {
            return Arrays.asList(parameters);
        }

        return null;
    }

    public void setParameters(Object[] parameters) {
        this.parameters = parameters;
    }
}
