package com.bexs.route.persistence.exception;

/**
 * Enum para agrupar as mensagens de erro uilizadas nas exceções.
 *
 * @author bruno
 */
public enum ErrorCode {

    EXP_1("EXP-1", "Internal error."),
    EXP_2("EXP-2", "File not found or file opened. Make sure it remains in the correct place or close the file."),
    EXP_3("EXP-3", "Reading/Writing error: Check if the file is available."),
    ERR_2("ERR-2", "Formatting error: Check if the fields in the file are correct. EX: AAA;BBB;10");

    private final String code;
    private final String message;

    ErrorCode(final String code, final String message) {
        this.code = code;
        this.message = message;
    }

    @Override
    public String toString() {
        return code + ": " + message;
    }

    public String getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

}
