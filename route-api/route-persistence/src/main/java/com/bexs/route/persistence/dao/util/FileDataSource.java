package com.bexs.route.persistence.dao.util;

import java.io.BufferedReader;
import java.io.FileReader;
import java.math.BigDecimal;

/**
 * Classe suporte para análise da fote de dados.
 *
 * @author bruno
 */
public class FileDataSource {

    /**
     * Valida os padrões do arquivo de entrada.
     *
     * @param dataSource <-- Endereço apra coleta de dados
     * @return valid
     */
    public static boolean validDatasourceFile(final String dataSource) {
        try {
            final BufferedReader reader = new BufferedReader(new FileReader(dataSource));
            String line = reader.readLine();
            while (line != null) {
                String[] routeStr = line.split(";");
                if (routeStr.length == 3 && routeStr[0].matches("[a-zA-Z]{3}") && routeStr[1].matches("[a-zA-Z]{3}")) {
                    new BigDecimal(routeStr[2]);
                } else {
                    reader.close();
                    throw new Exception();
                }
                line = reader.readLine();
            }
            reader.close();
        } catch (final Exception e) {
            return false;
        }
        return true;
    }

}
