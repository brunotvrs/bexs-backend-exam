package com.bexs.route.persistence.entity;

import java.math.BigDecimal;

/**
 * Classe representando a entidade rota. Para futuras implementações de acesso
 * aos dados, essa classe poderá ser mapeada em relação ao banco de dados.
 * 
 * @author bruno
 *
 */
public class Route {

	private String fromLocation;
	private String toLocation;
	private BigDecimal price;

	public Route() {
	}

	public Route(String fromLocation, String toLocation, BigDecimal price) {
		this.fromLocation = fromLocation;
		this.toLocation = toLocation;
		this.price = price;
	}

	public String getFromLocation() {
		return fromLocation;
	}

	public void setFromLocation(final String fromLocation) {
		this.fromLocation = fromLocation;
	}

	public String getToLocation() {
		return toLocation;
	}

	public void setToLocation(final String toLocation) {
		this.toLocation = toLocation;
	}

	public String getFullRoute() {
		return fromLocation + "-" + toLocation;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(final BigDecimal price) {
		this.price = price;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj != null && this.getFullRoute() != null)
			return this.getFullRoute().equals(((Route) obj).getFullRoute());
		return super.equals(obj);
	}

}
