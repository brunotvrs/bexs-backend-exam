package com.bexs.route.persistence.dao;

import java.util.List;

import com.bexs.route.persistence.entity.Route;

/**
 * Interface com os métodos de acesso aos dados de rota.
 *
 * @author bruno
 */
public interface RouteDAO {

    /**
     * Salva ou atualiza uma rota existente.
     *
     * @param route      <-- Entity de rota
     * @param dataSource <-- Endereço apra coleta de dados
     */
    void saveUpdate(final Route route, final String dataSource);

    /**
     * Lista todas as rotas existentes.
     *
     * @param dataSource <-- Endereço apra coleta de dados
     * @return routes
     */
    List<Route> list(final String dataSource);
}
