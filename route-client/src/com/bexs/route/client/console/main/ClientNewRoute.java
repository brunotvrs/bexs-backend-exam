package com.bexs.route.client.console.main;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Scanner;

/**
 * App console cliente que consome a API REST via POST para a criação de novas
 * rotas.
 *
 * @author bruno
 *
 */
public class ClientNewRoute {

	private static final String ENDPOINT_NEW_ROUTE = "http://localhost:8080/route-api/route";
	private static final int HTTP_SUCCESS = 200;

	/**
	 * Método principal de interação com o usuário. Para encerrar a aplicação
	 * digite: exit
	 *
	 * @param args <-- Params de inicialização
	 */
	@SuppressWarnings("resource")
	public static void main(String[] args) {
		try {

			final Scanner sc = new Scanner(System.in);
			System.out.print("Please enter the new route (AAA,BBB,10):");
			final String route = sc.next().trim();
			if ("EXIT".equalsIgnoreCase(route))
				System.exit(0);

			if (validInput(route)) {
				final HttpURLConnection request = sendRequest(inputToJson(route));
				printResponse(request);
			} else
				System.out.println("Formatting error: Try the pattern \"AAA,BBB,00\"");
		} catch (final Exception e) {
			if ("Connection refused: connect".equals(e.getMessage())) {
				System.out.println("Unable to open the connection: Check if the route-api is online!");
			} else if (e.getMessage().contains("ERR"))
				System.out.println(e.getMessage());
			else if (e.getMessage().contains("EXP"))
				System.out.println("Server error.");
			else
				System.out.println("Internal error.");
		}
		run();
	}

	/**
	 * Método que estabelece a conexão com o serviço.
	 *
	 * @param content <-- Conteúdo capturado na tela
	 * @return request
	 * @throws Exception <-- Erro na abertura da conexão
	 */
	private static HttpURLConnection sendRequest(final String content) throws Exception {
		final URL url = new URL(ENDPOINT_NEW_ROUTE);
		final HttpURLConnection request = (HttpURLConnection) url.openConnection();
		request.setDoOutput(true);
		request.setDoInput(true);
		request.setRequestProperty("Content-Type", "application/json");
		request.setRequestMethod("POST");
		try (OutputStream outputStream = request.getOutputStream()) {
			outputStream.write(content.getBytes(StandardCharsets.UTF_8));
		}
		return request;
	}

	/**
	 * Converte a entrada no JSON de chamada.
	 *
	 * @param route <-- Conteúdo capturado na tela
	 * @return url
	 */
	private static String inputToJson(final String route) {
		final String[] spltRoute = route.split(",");
		return "{ \"fromLocation\" : \"" + spltRoute[0] + "\", \"toLocation\" : \"" + spltRoute[1] + "\" , \"price\" : "
				+ spltRoute[2] + " }";
	}

	/**
	 * Valida a entrada de dados.
	 *
	 * @param route <-- Conteúdo capturado na tela
	 * @return <-- Válido ou não
	 */
	private static boolean validInput(final String route) {

		if (route != null) {
			final String[] spltRoute = route.split(",");
			if (spltRoute.length == 3) {
				if (!spltRoute[0].matches("[A-Z]{3}") || !spltRoute[1].matches("[A-Z]{3}"))
					return false;
				try {
					new BigDecimal(spltRoute[2].replace(",", "."));

				} catch (final Exception e) {
					return false;
				}
			} else
				return false;
			return true;
		}
		return false;
	}

	/**
	 * Método para leitura e exibição do resultado da chamada.
	 *
	 * @param request <-- HttpRequest
	 * @throws Exception <-- Erro no recebimento da resposta
	 */
	private static void printResponse(HttpURLConnection request) throws Exception {
		BufferedReader reader;
		if (request.getResponseCode() != HTTP_SUCCESS) {
			reader = new BufferedReader(new InputStreamReader(request.getErrorStream()));
			throw new RuntimeException(reader.readLine());
		}
		reader = new BufferedReader(new InputStreamReader((request.getInputStream())));
		String line = reader.readLine();
		while (line != null) {
			System.out.println(line);
			line = reader.readLine();
		}
		reader.close();
		request.disconnect();
	}

	/**
	 * Mantém o programa ativo para novas operações.
	 *
	 */
	private static void run() {
		main(null);
	}
}
