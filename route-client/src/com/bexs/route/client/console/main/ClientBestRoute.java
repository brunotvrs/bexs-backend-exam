package com.bexs.route.client.console.main;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Scanner;

/**
 * App console cliente que consome a API REST via GET para consulta de melhor
 * preço.
 *
 * @author bruno
 */
public class ClientBestRoute {

	private static final String ENDPOINT_SEARCH = "http://localhost:8080/route-api/best-price-route/";
	private static final int HTTP_SUCCESS = 200;

	/**
	 * Método principal de interação com o usuário. Para encerrar a aplicação
	 * digite: exit
	 *
	 * @param args <-- Params de inicialização
	 */
	@SuppressWarnings("resource")
	public static void main(String[] args) {
		try {

			final Scanner sc = new Scanner(System.in);
			System.out.print("Please enter the route:");
			final String route = sc.next().trim();
			if ("EXIT".equalsIgnoreCase(route))
				System.exit(0);

			final HttpURLConnection request = sendRequest(route);
			printResponse(request);
		} catch (final Exception e) {
			if ("Connection refused: connect".equals(e.getMessage())) {
				System.out.println("Unable to open the connection: Check if the route-api is online!");
			} else if (e.getMessage().contains("ERR"))
				System.out.println(e.getMessage());
			else if (e.getMessage().contains("EXP"))
				System.out.println("Server error.");
			else
				System.out.println("Internal error.");
		}
		run();
	}

	/**
	 * Método que estabelece a conexão com o serviço.
	 *
	 * @param content <-- Conteúdo capturado na tela
	 * @return request
	 * @throws Exception <-- Erro na abertura da conexão
	 */
	private static HttpURLConnection sendRequest(final String content) throws Exception {

		if (content != null && content.contains("-")) {
			String fromTo = content.toUpperCase().replace("/n", "").replace("/r", "").trim();
			if (fromTo.matches("[a-zA-Z]{3}-[a-zA-Z]{3}")) {

				final String[] spltfromTo = content.split("-");
				fromTo = "fromLocation=" + spltfromTo[0] + "&toLocation=" + spltfromTo[1];

				final URL url = new URL(ENDPOINT_SEARCH + fromTo);
				return (HttpURLConnection) url.openConnection();
			}

		}
		throw new Exception("ERR-1\", \"Formatting error: Try the pattern \"AAA-AAA\"");
	}

	/**
	 * Método para leitura e exibição do resultado da chamada.
	 *
	 * @param request <-- HttpRequest
	 * @throws Exception <-- Erro no recebimento da resposta
	 */
	private static void printResponse(HttpURLConnection request) throws Exception {
		BufferedReader reader;
		if (request.getResponseCode() != HTTP_SUCCESS) {
			reader = new BufferedReader(new InputStreamReader(request.getErrorStream()));
			throw new RuntimeException(reader.readLine());
		}
		reader = new BufferedReader(new InputStreamReader((request.getInputStream())));
		String line = reader.readLine();
		while (line != null) {
			System.out.println(line);
			line = reader.readLine();
		}
		reader.close();
		request.disconnect();
	}

	/**
	 * Mantém o programa ativo para novas operações.
	 */
	private static void run() {
		main(null);
	}
}
