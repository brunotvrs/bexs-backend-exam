# Informações Iniciais
Olá, seguem as características da solução:

* Linguagem _Java_
* _API Rest_ com _Spring Boot_
    * Foi minha escolha por gerar uma aplicação console com um servidor web _embedded_, isso facilitará a instalação e os testes para a nosso bate-papo.
* _Maven_
    * Para gerenciar builds e dependências
* _JUnit_
* Desenvolvido na _IDE Eclipse 2020_

---

## Decisões de Arquitetura

> _CLIENTS <-> CONTAINER <-> DATA_


O _CONTAINER_ foi modularizado visando escalabilidade: criação de novos módulos, novos projetos web e etc.

Para aplicar a modularização foram criados:

* route-api
    * Parent agrupador dos demais módulos, gerencia dependências e builds
* route-core
    * Contendo os endpoints, tratamento de fronteiras e aplicação de regras de negócio
* route-persistence
    * Contendo os acessos de dados e entidades

### Resumo

1. Qualquer cliente envia sua requisição (_HttpRequest_) para o _CONTAINER_
2. O _CONTAINER_ recebe através do _route-core_
3. O _CONTAINER_ processa a requisição utilizando ou não os acessos aos dados (_DATA_)
4. O _CONTAINER_ devolve (_HttpResponse_) referente à requisição
5. O _DATA_ pode ser qualquer armazenamento, desde um simples arquivo até vários bancos de dados distribuídos


> Obs: Para rodar apenas as funcionalidades requisitadas no desafio, algo mais enxuto solucionaria a questão. Entretanto, minha ideia foi enriquecer nosso bate-papo trazendo algo um pouco mais robusto ao debate.

---

## Decisões de Design

### Para o route-core

> _Controller_ (opera requisições e modelos) <-> _Service_ (business e chamada de dados)

src\main\java
   
* _com.bexs.route.core_
    * Contém a classe com o método main da aplicação
* _com.bexs.route.core.common.controller_
    * Controladores - endpoints
* _com.bexs.route.core.common.service_
    * Serviços - business
* _com.bexs.route.core.common.model_
    * Modelos - data transfer object
* _com.bexs.route.core.common.exception_
    * Exceções
* _com.bexs.route.core.common.enums_
    * Enumeradores
* _com.bexs.route.core.common.util_
    * Utilitários
    
src\test\java
    
* _com.bexs.route.core.common.controller_
    * Testes unitários dos controladores
* _com.bexs.route.core.common.service_
    * Testes unitários dos serviços


### Para o route-persistence

> _Data Access Object_ (interfaces de acesso aos dados) <-> _DAOImpl_ (implementações das interfaces) <-> _Entity_ (objetos mapeadores)

src\main\java

* _com.bexs.route.persistence.dao_
    * Interfaces
* _com.bexs.route.persistence.dao.impl_
    * Implementação das interfaces - Implementei acessando o arquivo como base de dados
* _com.bexs.route.persistence.entity_
    * Entidades
* _com.bexs.route.persistence.exception_
    * Exceções
* _com.bexs.route.persistence.dao.util_
    * Utilitários

#### Resumo

1. O _Controller_ oferece os endpoints
2. O _Service_, chamado pelo _Controller_, executa operações de negócio
3. Se necessário, o _Service_ aciona o "_route-persistence_" através da interface _DAO_ escolhendo uma implementação
4. A opção por _DAO_ e _DAOImpl_ foi para gerar escalabilidade na escolha pela base de dados (arquitetura do _DATA_)
5. A _Entity_ receberá o mapeamento com base na escolha pela base de dados (arquitetura do _DATA_)

---

## Desenvolvimento / Importação dos Projetos / Validação de Código

### Desenvolvimento

Utilizei _BDD (Behavior Driven Development)_ como guia para o desenvolvimento, implementei e orientei meus testes para o comportamento esperado de cada funcionalidade (testei unitáriamente apenas os controladores e serviços).

Descartei a utilização do _TDD_ (Test Driven Development). Creio que o objetivo principal do desafio proposto por vocês não seja validar testes mais detalhados (testes por método em todas as classes). 
Para isso, precisaria fazer o uso intensivo de API de terceiros (Mock, MockMVC, Mockito e etc), apostei que fugiria do escopo para esse momento de bate-papo. 

### Como Importar os Projetos no Ambiente de Desenvolvimento

1. Escolha a IDE de sua preferência (Eclipse, IntelliJ, ...)
2. Importe os projetos como _Maven Project_
3. Automaticamente ele resolverá as dependências (caso não resolva, acione o _Maven Build_)
4. Para rodar na "IDE": _"Run" ou "Debug"_ na _classe "com.bexs.route.core.CoreApplication"_ (presente no "_route-core_") não esquecendo de passar nos argumentos (_Run Configurations... > Arguments_) o endereço físico do _input-file_ (Ex: _C:\input-file.csv_) <- Evite caminhos com espaço

#### Gerando o Executável

1. No projeto "_route-api_" acione o comando "_Maven Install_"
2. Ao final do processamento, na raiz da pasta "_target_" do projeto "_route_core_" estará o arquivo "_route-core.jar_"

### Validação de Código

Seguem algumas observações:

* Não utilizei um _checkstyle_ diferente do padrão da IDE para formatação do código (sei que cada empresa tem o seu)
* Todos os métodos foram comentados para a geração do JavaDoc das aplicações (comentei os métodos em português para auxiliar no nosso bate-papo)
* Fiz algumas coisas "_no braço_" que poderiam ter sido feitas por APIs de terceiros (por exemplo, criação de _JSON_)
* Nos testes, para não criar e operar excessivamente "_Mocks_" de serviço dentro dos métodos, simulei um arquivo de dados para consultas e registros

---

## Documentação _API REST_ (_route-api_)

Funcionalidades:

* Interface de consulta pela rota de menor preço
* Interface para inserir uma nova rota no _input-file_

### Interface de consulta pela rota de menor preço

#### Endpoint: http://localhost:8080/route-api/best-price-route/fromLocation=AAA&toLocation=BBB

* **Para uma rota encontrada:**
    * _"Best route: AAA-CCC-DDD-BBB > $10.0"_
     * _Response HttpStatus: 200 OK_
* **Para mais de uma rota de mesmo preço:**
    * _"Best route: AAA-CCC-BBB > $10.0 OR AAA-DDD-BBB > $10.0 OR AAA-EEE-BBB > $10.0"_
    * _Response HttpStatus: 200 OK_
* **Para uma rota não encontrada:**
    * _"No route available!"_
    * _Response HttpStatus: 200 OK_
* **Para parâmetros inválidos:**
    * _"ERR-1: Formatting error: Try the pattern "AAA-AAA""_
    * _Response HttpStatus: 400 Bad Request_
* **Para arquivo de dados não encontrado ou aberto:**
    * "_EXP-2: File not found or file opened. Make sure it remains in the correct place or close the file._"
    * _Response HttpStatus: 500 Internal Server Error_


### Interface para inserir uma nova rota no _input-file_

#### Endpoint: http://localhost:8080/route-api/route
* **Recebe um _JSON_ no _Body_ da requisição:**
    * ``` { "fromLocation" : "AAA", "toLocation" : "BBB", "price" : 10 } ```
* **Para uma rota inserida:**
    * "_Success!_"
    * _Response HttpStatus: 200 OK_
* **Para o _value_ "fromLocation" inválido:**
    * "_ERR-2: Formatting error: Field "fromLocation" incorrect! Try: "AAA"_"
    * _Response HttpStatus: 400 Bad Request_
* **Para o _value_ "toLocation" inválido:**
    * "_ERR-3: Formatting error: Field "toLocation" incorrect! Try: "AAA"_" 
    * _Response HttpStatus: 400 Bad Request_
* **Para o _value_ "price" inválido:**
    * "_ERR-4: Formatting error: Field "price" incorrect! Try: "40", "40.0" or "40,00"_"
    * _Response HttpStatus: 400 Bad Request_
* **Para _keys_ inválidas:**
    * "_ERR-5: Formatting error: Malformed JSON. Check keys and values_"
    * _Response HttpStatus: 400 Bad Request_
* **Para arquivo de dados não encontrado ou aberto:**
    * "_EXP-2: File not found or file opened. Make sure it remains in the correct place or close the file._"
    * _Response HttpStatus: 500 Internal Server Error_
---

## Execução

Aqui a razão pela qual escolhi o Spring Boot para essa avaliação.

Você precisará apenas:

* Java instalado na máquina a qual subirá o servidor
* Do arquivo "_route-core.jar_"
    * Veja como gerar no item: _"Desenvolvimento / Importação dos Projetos / Validação de Código" > "Gerando o Executável"_

Se o java estiver no _classpath_ do seu sistema operacional navegue até o diretório no qual se encontra o arquivo "_route-core.jar_" e execute:

* ```java -jar route-core.jar C:\input-file.csv```
* Não esqueça de passar o endereço do _input-file_ como primeiro argumento de execução.

Caso não esteja no _classpath_, adicione o arquivo "_route-core.jar_" na pasta "_bin_" do Java instalado e execute o mesmo comando: <- AÇÃO FORTEMENTE NÃO RECOMENDADA

* ```java -jar route-core.jar C:\input-file.csv```
 
---
## Aplicações Console

Aqui eu fugi um pouco do requisito para trazer algo a mais a nossa discussão.
Ao invés de rodar "_stand-alone_" com a funcionalidade "_embedded_", achei justo que o console consumisse a API Rest que acabamos de criar.

Para isso implementei uma classe para cada _endpoint_ criado.

As aplicações se encontram no projeto "_route-client_".
São 2 classes executáveis que rodam sozinhas sem dependência alguma.


As classes são:

* ClientBestRoute.java
* ClientNewRoute.java

> Para o nosso contexto, achei exagerado criar "_.jar(s)_" para cada um deles.

### Execução

Para executar, basta utilizar os comandos (mesma situação do _classpath_ citada anteriormente): 

* ```java ClientBestRoute.java```
* ```java ClientNewRoute.java```
* Para que funcione adequadamente, suba a API Rest antes desas execuções.
* Todas possuem um comando "exit" para encerrar a aplicação

---

## Considerações Finais

Agradeço a oportunidade de codificar para vocês, peço desculpas pelos desvios de execução, minha intenção foi valorizar ao máximo o nosso bate-papo.

Caso tenham qualquer dúvida de ambiente, instalação ou processo, é só me chamar!

* Bruno Tavares Amaral
* (41) 99642-5450 (WhatsApp)
* bruno.tavares@outlook.com

Até mais!


